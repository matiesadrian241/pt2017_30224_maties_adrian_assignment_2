import java.util.Random;
public class Simulare implements Runnable{
	public GUI frame= new GUI();
	public int tminServire;
	public int tmaxServire;
	public int tminSosire;
	public int tmaxSosire;
	public int nrCozi;
	public int interval;
	public int timpcurent;
	private Thread t;
	public int ind;
	public int sos1;
	private boolean running;
	public boolean ok;
	public Server[] s;
	public Random random;
	public Simulare(int tminServire,int tmaxServire,int tminSosire,int tmaxSosire,int nrCozi,int interval,boolean ok,GUI frame){
		this.tminServire=tminServire;
		this.tmaxServire=tmaxServire;
		this.tminSosire=tminSosire;
		this.tmaxSosire=tmaxSosire;
		this.nrCozi=nrCozi;
		this.interval=interval;
		this.ok=ok;
		this.frame=frame;
		random=new Random();
		t=new Thread(this);
		running=false;
		s=new Server[nrCozi];
		for(int i=0;i<nrCozi;i++){
			s[i]=new Server(i,frame);
		}
	}
	public Server[] getServers(){
		return s;
	}
	public int random(int min, int max){
		return (random.nextInt(max-min+1)+min);
	}
	public void start(){
		running=true;
		t.start();
		for(int i=0;i<nrCozi;i++){
			s[i].start();
		}
	}
	public int getTimpMinim(){
		float minim=s[0].timpServire();
		int index=0;
		for(int i=1;i<s.length;i++){
			if(s[i].timpServire()<minim){
			minim=s[i].timpServire();
			index=i;
			}
		}
		return index;
	}
	public int getMinClienti(){
		int minim=s[0].getLungimeCoada();
		int index=0;
		for(int i=1;i<s.length;i++){
			if(s[i].getLungimeCoada()<minim){
				minim=s[i].getLungimeCoada();
				index=i;
			}
		}
		return index;
	}
	//public int sosFunc(){
		//return sos1;
	//}
	@Override
	public void run() {
		System.out.println("Simularea a inceput");
		int coada;
		int serv;
		int sos;
		while(timpcurent<interval){
			if(ok==false){
				timpcurent=timpcurent+random(tminSosire,tmaxSosire);// Cand ok este FALSE se vor distribui clientii in functie de timpul cel mai mic de asteptare
				try {
					Thread.sleep(random(tminSosire,tmaxSosire)*1000);
				} catch (InterruptedException e) {
						e.printStackTrace();
					}
				serv=random(tminServire,tmaxServire);
				sos=random(tminSosire,tmaxSosire);
				Client x= new Client(sos,serv);
				coada=getTimpMinim();
				s[coada].addClient(x);
				frame.updateadd(String.valueOf(s[coada].getIndex()+1),sos,serv);
				System.out.println("Clientul a fost adaugat in coada"+(coada+1));
			}
			else //Cand ok este TRUE se vor distribui clientii in functie de locurile minime ocupate
			{
				timpcurent=timpcurent+random(tminSosire,tmaxSosire);
				try {
					Thread.sleep(random(tminSosire,tmaxSosire)*1000);
				} catch (InterruptedException e) {
						e.printStackTrace();
					}
				serv=random(tminServire,tmaxServire);
				sos=random(tminSosire,tmaxSosire);
				Client x= new Client(sos,serv);
				coada=getMinClienti();
				s[coada].addClient(x);
				frame.updateadd(String.valueOf(s[coada].getIndex()+1),sos,serv);
				System.out.println("Clientul a fost adaugat in coada "+(coada+1));
			}}
		for(int i=0;i<s.length;i++){
			s[i].stop();
		}
		System.out.println("Simularea s-a terminat");
	}
}
