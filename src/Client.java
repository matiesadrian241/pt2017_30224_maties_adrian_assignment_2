public class Client {
	private int  timpSosire;
	private int timpProcesare;
	
	public Client(int a,int b)
	{
		this.timpSosire = a;
		this.timpProcesare = b;
	}
	public int getTimpSosire() {
		return this.timpSosire;
	}
	public String toString(){
		return "Client: "+" " +Integer.toString(timpProcesare)+ " "+ Integer.toString(timpSosire)+" ";
	}
	public int getTimpProcesare() {
		return this.timpProcesare;
	}
	public void setTimpSosire(int t){
		this.timpSosire=t;
	}
	public void setTimpProcesare(int t){
		this.timpProcesare=t;
	}

}
