import java.awt.EventQueue;
import java.awt.*;
import javax.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.*;
import java.util.Random;
public class GUI extends JFrame{
	private JTextField textField;
	private JTextField[] output = new JTextField[10];
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JPanel frame;
	public String[] sir;
	private JTextArea textArea;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	private boolean ok;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the application.
	 */

	/**
	 * Initialize the contents of the frame.
	 */
	/*public void displayData (ArrayList <Server> x) {
		textField.setText("");
		textField.revalidate();
		for(Server s: x) {
			for (Client i : s.getTask()) {
				textField.setText("*");
			}
			textField.setText("\n");
		}
		textField.setText("\n");
		textField.revalidate();
	}*/
	void setare(int a,int b,String[] c){
		sir=c;
		
		//String y="( ͡ᵔ ͜ʖ ͡ᵔ )";
		
		for(int i=0;i<a;i++){
			output[b].setText(sir[i]);
		}
		//System.out.println(b+" "+sir);
	}
	void updatefinal(int x,String y){
		textArea.append("# Timpul mediu de asteptare pentru coada "+ y + " a fost de "+x+" secunde #\n");
	}
	void updateadd(String s,int a,int b){
		textArea.append("Un client a fost adaugat in coada: "+s+" cu AT: "+a+" si ST: "+b +"\n");
	}
	void updateremove(String s,int y){
		textArea.append("Clientul iesit din coada "+s + " dupa " + y + " secunde de asteptare"+"\n");
	}
	public GUI() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 960, 403);
		frame = new JPanel();
		frame.setBorder(new EmptyBorder(10,10,10,10));
		setContentPane(frame);
		frame.setLayout(null);
		JLabel lblNumarCozi = new JLabel("Numar cozi:");
		lblNumarCozi.setBounds(10, 11, 68, 14);
		frame.add(lblNumarCozi);
		
		JLabel lblTimpMinimSosire = new JLabel("Timp minim sosire:");
		lblTimpMinimSosire.setBounds(10, 36,100, 14);
		frame.add(lblTimpMinimSosire);
		
		JLabel lblTimpMaximSosire = new JLabel("Timp maxim sosire:");
		lblTimpMaximSosire.setBounds(10, 61, 100, 14);
		frame.add(lblTimpMaximSosire);
		
		JLabel label = new JLabel("");
		label.setBounds(10, 86, 46, 14);
		frame.add(label);
		
		JLabel lblTimpMinimSosire_1 = new JLabel("Timp minim servire:");
		lblTimpMinimSosire_1.setBounds(219, 36, 100, 14);
		frame.add(lblTimpMinimSosire_1);
		
		JLabel lblTimpMaximSosire_1 = new JLabel("Timp maxim servire:");
		lblTimpMaximSosire_1.setBounds(219, 61, 100, 14);
		frame.add(lblTimpMaximSosire_1);
		
		JLabel lblIntervalSimulare = new JLabel("Interval simulare:");
		lblIntervalSimulare.setBounds(219, 11, 100, 14);
		frame.add(lblIntervalSimulare);
		
		JLabel lblThread = new JLabel("Server #1");
		lblThread.setBounds(10, 111, 68, 14);
		frame.add(lblThread);
		
		JLabel lblServer = new JLabel("Server #2");
		lblServer.setBounds(10, 136, 68, 14);
		frame.add(lblServer);
		
		JLabel lblServer_1 = new JLabel("Server #3");
		lblServer_1.setBounds(10, 161, 68, 14);
		frame.add(lblServer_1);
		
		JLabel lblServer_2 = new JLabel("Server #4");
		lblServer_2.setBounds(10, 186, 68, 14);
		frame.add(lblServer_2);
		
		JLabel lblThread_1 = new JLabel("Server #5");
		lblThread_1.setBounds(10, 211, 68, 14);
		frame.add(lblThread_1);
		
		JLabel lblServer_3 = new JLabel("Server #6");
		lblServer_3.setBounds(10, 236, 68, 14);
		frame.add(lblServer_3);
		
		JLabel lblServer_4 = new JLabel("Server #7");
		lblServer_4.setBounds(10, 261, 68, 14);
		frame.add(lblServer_4);
		
		JLabel lblServer_5 = new JLabel("Server #8");
		lblServer_5.setBounds(10, 286, 68, 14);
		frame.add(lblServer_5);
		
		JLabel lblServer_6 = new JLabel("Server #9");
		lblServer_6.setBounds(10, 311, 68, 14);
		frame.add(lblServer_6);
		
		JLabel lblServer_7 = new JLabel("Server #10");
		lblServer_7.setBounds(10, 336, 68, 14);
		frame.add(lblServer_7);
		
		output[0] = new JTextField();
		output[0].setBounds(88, 108, 318, 20);
		frame.add(output[0]);
		output[0].setColumns(10);
		
		output[1] = new JTextField();
		output[1].setBounds(88, 133, 318, 20);
		frame.add(output[1]);
		output[1].setColumns(10);
		
		output[2] = new JTextField();
		output[2].setBounds(88, 158, 318, 20);
		frame.add(output[2]);
		output[2].setColumns(10);
		
		output[3] = new JTextField();
		output[3].setBounds(88, 183, 318, 20);
		frame.add(output[3]);
		output[3].setColumns(10);
		
		output[4] = new JTextField();
		output[4].setColumns(10);
		output[4].setBounds(88, 208, 318, 20);
		frame.add(output[4]);
		
		output[5] = new JTextField();
		output[5].setColumns(10);
		output[5].setBounds(88, 233, 318, 20);
		frame.add(output[5]);
		
		output[6] = new JTextField();
		output[6].setColumns(10);
		output[6].setBounds(88, 258, 318, 20);
		frame.add(output[6]);
		
		output[7] = new JTextField();
		output[7].setColumns(10);
		output[7].setBounds(88, 283, 318, 20);
		frame.add(output[7]);
		
		output[8]= new JTextField();
		output[8].setColumns(10);
		output[8].setBounds(88, 308, 318, 20);
		frame.add(output[8]);
		
		output[9]= new JTextField();
		output[9].setColumns(10);
		output[9].setBounds(88, 333, 318, 20);
		frame.add(output[9]);
		
		textField_10 = new JTextField();
		textField_10.setBounds(112, 8, 86, 20);
		frame.add(textField_10);
		textField_10.setColumns(10);
		
		textField_11 = new JTextField();
		textField_11.setColumns(10);
		textField_11.setBounds(112, 33, 86, 20);
		frame.add(textField_11);
		
		textField_12 = new JTextField();
		textField_12.setColumns(10);
		textField_12.setBounds(112, 58, 86, 20);
		frame.add(textField_12);
		
		textField_13 = new JTextField();
		textField_13.setColumns(10);
		textField_13.setBounds(320, 8, 86, 20);
		frame.add(textField_13);
		
		textField_14 = new JTextField();
		textField_14.setColumns(10);
		textField_14.setBounds(320, 33, 86, 20);
		frame.add(textField_14);
		
		textField_15 = new JTextField();
		textField_15.setColumns(10);
		textField_15.setBounds(320, 58, 86, 20);
		frame.add(textField_15);
		/*
		textField_16 = new JTextField();
		textField_16.setText("");
		textField_16.setBounds(469, 8, 465, 267);
		frame.add(textField_16);
		textField_16.setColumns(10);*/
		textArea = new JTextArea(10,267);
		//textArea.setEditable(false);
		//textArea.setBounds(469,8,450,267);
		/*
		textArea.setLineWrap(true);
		textArea.setVisible(true);*/
		textArea.setEditable(false);
		scrollPane = new JScrollPane(textArea);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(469,8,450,267);
		frame.add(scrollPane);
		frame.setVisible(true);
		//JScrollPane scrollPane = new JScrollPane(textArea);
		//textArea.setText("");
		frame.add(scrollPane);
		//frame.add(scrollPane);
		
		//frame.add(scrollPane);
	//	scrollPane = new JScrollPane(textArea); 
		//frame.add(scrollPane);
	//	textArea.setEditable(false);
		
		JButton btnStart = new JButton("START");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
				Simulare s=new Simulare(Integer.parseInt(textField_14.getText()),Integer.parseInt(textField_15.getText()),Integer.parseInt(textField_11.getText()),
				Integer.parseInt(textField_12.getText()),Integer.parseInt(textField_10.getText()),Integer.parseInt(textField_13.getText()),ok,GUI.this);
				//textField_2.setText("*");
				s.start();
				//textField.setText("sasa");
				}catch(NumberFormatException e) {}
			}
		});
		btnStart.setBounds(653, 291, 226, 54);
		frame.add(btnStart);
		
		JRadioButton rdbtnStrategieLocuri_1 = new JRadioButton("Strategie: locuri");
		rdbtnStrategieLocuri_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				ok=true;
			}
		});
		rdbtnStrategieLocuri_1.setBounds(479, 327, 109, 23);
		frame.add(rdbtnStrategieLocuri_1);
		JRadioButton radioButton = new JRadioButton("Strategie: timp");
		radioButton.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				ok=false;
			}
		});
		radioButton.setBounds(479, 286, 109, 39);
		frame.add(radioButton);
	}
}