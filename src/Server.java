
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
	private ArrayList<Client> clienti;
	public GUI frame= new GUI();
	public int medie=0,sum=0,nr=03;
	private AtomicInteger timpAsteptare;
	Random r=new Random();
	public boolean running;
	private Thread t;
	public int var=0;
	private List<Server> servers = new ArrayList<Server>();
	private int index;
	public Server(int i, GUI frame)
	{
		this.index=i;
		t=new Thread(this);
		this.clienti=new ArrayList<Client>();
		this.timpAsteptare= new AtomicInteger(0);	
		this.frame=frame;
	}
	public void addClient(Client task)
	{
		clienti.add(task);
		frame.setare(clienti.size(),index,getClienti());
		//
		//rame.setare(2,3);
		//frame.update();
	}
	public ArrayList<Client> getServers(){
		return clienti;
	}
	public synchronized ArrayList<Client> getTask(){
		ArrayList<Client> array = new ArrayList<Client>();
		for(Client t: clienti)
		{
			array.add(t);
		}
		return array;
	}
	public String[] getClienti(){
		String[] task=new String[clienti.size()+1];
		task[0]=String.format("Server %d",timpAsteptare.get());
		for(int i=1;i<clienti.size();i++){
			task[i]=clienti.toString();
		}
		return task;
	}
	public void start(){
		running=true;
		t.start();
	}
	public void stop(){
		running=false;
	}
	public int getIndex(){
		return index;
	}
	public void run()
	{
		while(running || clienti.size()>0)
		{
			
			if(clienti.size()>0)
			{
				/*int x=clienti.get(0).getTimpProcesare();
				System.out.println("---------------------"+x);
				clienti.remove(0);
				frame.setare(clienti.size(),index);
				frame.updateremove(String.valueOf(index));
			System.out.println("Clientul a fost scos din coada");*/
			try{
				int x=clienti.get(0).getTimpProcesare();
				System.out.println("---------------------"+x);
				Thread.sleep(x*1000);
				var+=x;
				nr++;
				medie=var+medie;
				clienti.remove(0);
				frame.setare(clienti.size(),index,getClienti());
				frame.updateremove(String.valueOf(index+1),(var));
				System.out.println("Clientul a iesit din coada");
			}catch(InterruptedException e)
			{
				e.printStackTrace();
			}}
			else {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	
		}
		frame.updatefinal(medie/nr,String.valueOf(index+1));
	}
	
	public int getLungimeCoada()
	{
		return this.clienti.size();
	}
	
	//public float getTimpAstepare()
	//{
		//return this.timpAsteptare.floatValue();	
	//}
	public int timpServire(){
		int timp=0;
		for(int i=0;i<clienti.size();++i){
			Client x=clienti.get(i);
			timp=timp+x.getTimpProcesare();
		}
		return timp;
	}
}
